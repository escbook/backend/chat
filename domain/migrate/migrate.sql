CREATE DATABASE "eb_chat";

CREATE SCHEMA IF NOT EXISTS "eb_chat"."develop";

CREATE TABLE "eb_chat"."develop"."chat_rooms" (
    "id" VARCHAR PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "eb_chat"."develop"."user_chat_rooms" (
    "user_id" VARCHAR NOT NULL,
    "chat_room_id" VARCHAR NOT NULL,
    "joined_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("user_id", "chat_room_id"),
    FOREIGN KEY ("user_id") REFERENCES "eb_chat"."develop"."chat_rooms" ("id") ON DELETE CASCADE,
    FOREIGN KEY ("chat_room_id") REFERENCES "eb_chat"."develop"."chat_rooms" ("id") ON DELETE CASCADE
);

CREATE TABLE "eb_chat"."develop"."messages" (
    "id" SERIAL PRIMARY KEY,
    "chat_room_id" VARCHAR NOT NULL,
    "user_id" VARCHAR NOT NULL,
    "message" TEXT NOT NULL,
    "send_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY ("chat_room_id") REFERENCES "eb_chat"."develop"."chat_rooms" ("id") ON DELETE CASCADE,
    FOREIGN KEY ("user_id") REFERENCES "eb_chat"."develop"."chat_rooms" ("id") ON DELETE CASCADE
);

CREATE INDEX "idx_messages_chat_room_id" ON "eb_chat"."develop"."messages" ("chat_room_id");
CREATE INDEX "idx_messages_user_id" ON "eb_chat"."develop"."messages" ("user_id");